import java.awt.Point;
import java.util.ArrayList;

/**
 * Represents a sheep in the pasture.
 * @author Andreas Andersson, anan8777, andreas@biffnet.se
 * @edited 2015-05-04
 */
public class Sheep extends EntityMobile {
	
	private final static String IMAGE_URL = "images/sheep.gif";
	private final static boolean IS_ALIVE = true; 

	public Sheep(Pasture myPasture, int propagationTime) {
		super(myPasture, propagationTime, IMAGE_URL, IS_ALIVE, 
				Pasture.MOVE_DELAY_DEFAULT, Pasture.STARVE_TIME_SHEEP);
	}

	@Override
	public void tick() {
		
		mTimeLived++;

		Point myPos = mPasture.getPosition(this);
		if(myPos == null) {
			return;
		}
		
		// Eat Plant if there is any
		ArrayList<Entity> entitesAtThisPos = (ArrayList<Entity>) mPasture.getEntitiesAt(myPos);
		if(entitesAtThisPos != null) {
			for(Entity thisEntity : mPasture.getEntitiesAt(myPos)) {
				if(thisEntity instanceof Plant) {
					mPasture.removeEntity(thisEntity);
					mTimeToLive = Pasture.STARVE_TIME_SHEEP;
					mHasEaten = true;
				}
			}
		}
		
		// Checks whether this sheep has starved to death or not
		if(hasStarvedToDeath()) {
			System.out.println("Sheep died of starvation.");
			mPasture.removeEntity(this);
			return;
		}
				
		// One time unit has passed
		super.tick();
		
		
		
		// If its not my time, then do nothing
		if(mMoveDelay != 0) {
			return;
		} else {
			mMoveDelay = Pasture.MOVE_DELAY_DEFAULT;
		}
		
		// If this sheep sees a Wolf, it will run in the oposite direction
		ArrayList<Point> pointsInSight = getPointsInSight(myPos, mPasture.SIGHT_LENGTH_SHEEP);
		for(Point newPoint : pointsInSight) {
			ArrayList<Entity> pointList = (ArrayList<Entity>) mPasture.getEntitiesAt(newPoint);
			if(pointList != null) {
				for(Entity thisEntity : pointList) {
					if(thisEntity instanceof Wolf) {
						//Move to safe point
						Point dangerPos = mPasture.getEntityPosition(thisEntity);
						Point safePos = getSafePoint(myPos, dangerPos);
						if(safePos != null) {
							mCurrentCourse =  Course.getNewCourse(myPos, safePos);
							mPasture.moveEntity(this, safePos);
							System.out.println("Moving " + this + " away from " + thisEntity + ".");
							return;
						}
					}
				}
			}
		}
		
		// If Sheep sees a plant, move towards it.		
		for(Point newPoint : pointsInSight) {
			ArrayList<Entity> pointList = (ArrayList<Entity>) mPasture.getEntitiesAt(newPoint);
			if(pointList != null) {
				boolean goodPoint = false;
				for(Entity thisEntity : pointList) {
					if(thisEntity instanceof Plant) {
						goodPoint = true;
					} else {
						goodPoint = false;
						break;
					}
				}
				if(goodPoint) {
					System.out.println("Moving " + this + " towards plant.");
					mCurrentCourse =  Course.getNewCourse(myPos, newPoint);
					mPasture.moveEntity(this, newPoint);
					return;
				}
			}
		}
		
		// If the sheep sees nothing interesting, move in a similar direction as before
		Point similarCoursePoint = getSimilarCoursePoint(myPos);
		if(similarCoursePoint != null) {
			mCurrentCourse = Course.getNewCourse(myPos, similarCoursePoint);
			mPasture.moveEntity(this, similarCoursePoint);
			System.out.println("Moving " + this + " similar course.");
			return;
		}
		
		// If its not possible to move in i similar direction, move in a 
		// random direction
		goToRandom();
	}

	@Override
	public boolean isCompatible(Entity otherEntity) {
		if(otherEntity instanceof Sheep 
				|| otherEntity instanceof Fence
				|| otherEntity instanceof Wolf ) {
			return false;
		} else {
			return true;
		}
	}

}

import java.awt.Point;

/**
 * Class that represents a plant in the pasture.
 * @author Andreas Andersson, anan8777, andreas@biffnet.se
 * @edited 2015-05-04
 */
public class Plant extends PastureEntity {
	
	private final static String FENCE_IMAGE_URL = "images/plant.gif";
	private final static boolean IS_ALIVE = true; 

	public Plant(Pasture myPasture, int propagationTime) {
		super(myPasture, propagationTime, FENCE_IMAGE_URL, IS_ALIVE);
	}

	@Override
	public void tick() {
		
		// One time unit has passed
		if(mAlive) {
			mPropagationTime--;
		}
		
		// If its not my time, then do nothing
		if(mPropagationTime != 0) {
			return;
		}
		
		Point freeNeighbour = mPasture.getRandomFreeNeighbor(this);
        if(freeNeighbour != null) {
        	Plant newPlant = new Plant(mPasture, mPasture.PROPAGATION_TIME_PLANT);
        	mPasture.addEntity(newPlant, freeNeighbour);
        }

        mPropagationTime = mPasture.PROPAGATION_TIME_PLANT;
	}

	@Override
	public boolean isCompatible(Entity otherEntity) {
		if(otherEntity instanceof Fence || otherEntity instanceof Plant) {
			return false;
		} else {
			return true;
		}
	}

}

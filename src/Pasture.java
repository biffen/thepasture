import java.util.*;
import java.awt.Point;


/**
 * A pasture contains sheep, wolves, fences, plants, and possibly
 * other entities. These entities move around in the pasture and try
 * to find food, other entities of the same kind and run away from
 * possible enimies. 
 */
public class Pasture {
	
	// The time it takes before an entity can "reproduce"
	public final int PROPAGATION_TIME_PLANT = 10; //10
	public final int PROPAGATION_TIME_SHEEP = 101;
	public final int PROPAGATION_TIME_WOLF = 201;
	
	// How many units of time without food it takes
	// for the animals to starve
	public static final int STARVE_TIME_SHEEP = 100; //100
	public static final int STARVE_TIME_WOLF = 200; //200
	
	// How long the animals can "see" (scan boxes)
	public final int SIGHT_LENGTH_SHEEP = 2;
	public final int SIGHT_LENGTH_WOLF = 3;
	
	// Speed of the animals (how long between actions)
	public static final int MOVE_DELAY_DEFAULT = 10; 
	public static final int MOVE_DELAY_WOLF = 5;
	
	// How many of each entity there should be at start
	private final int STARTING_NUMBER_PLANT = 40; //40
	private final int STARTING_NUMBER_SHEEP = 20; //20
	private final int STARTING_NUMBER_WOLF = 4; //10

	
    private final int   width = 20;
    private final int   height = 20;

    //private final int   dummys = 20;

    private final Set<Entity> world = 
        new HashSet<Entity>();
    private final Map<Point, List<Entity>> grid = 
        new HashMap<Point, List<Entity>>();
    private final Map<Entity, Point> point 
        = new HashMap<Entity, Point>();

    private final PastureGUI gui;

    /** 
     * Creates a new instance of this class and places the entities in
     * it on random positions.
     */
    public Pasture() {

        Engine engine = new Engine(this);
        gui = new PastureGUI(width, height, engine);
        

        /* The pasture is surrounded by a fence. Replace Dummy for
         * Fence when you have created that class */
        
        for (int i = 0; i < width; i++) {
            addEntity(new Fence(this), new Point(i,0));
            addEntity(new Fence(this), new Point(i, height - 1));
        }
        for (int i = 1; i < height-1; i++) {
            addEntity(new Fence(this), new Point(0,i));
            addEntity(new Fence(this), new Point(width - 1,i));
        }

        
        /* 
         * Now insert the right number of different entities in the
         * pasture.
         */
        
        for (int i = 0; i < STARTING_NUMBER_PLANT; i++) {
        	Entity plant = new Plant(this, PROPAGATION_TIME_PLANT);
        	addEntity(plant, getFreePosition(plant));
        }
        
        for (int i = 0; i < STARTING_NUMBER_SHEEP; i++) {
        	Entity sheep = new Sheep(this, PROPAGATION_TIME_SHEEP);
        	addEntity(sheep, getFreePosition(sheep));
        }
        
        for (int i = 0; i < STARTING_NUMBER_WOLF; i++) {
        	Entity wolf = new Wolf(this, PROPAGATION_TIME_WOLF);
        	addEntity(wolf, getFreePosition(wolf));
        }

        gui.update();
    }

    public void refresh() {
        gui.update();
    }
    
    /**
     * Uses getFreeNeighbours to get all free points/neighbors, then it
     * returns a random point.
     */
    public Point getRandomFreeNeighbor(Entity toPlace) {
    	
    	List<Point> c = getFreeNeighbours(toPlace);
    	
        if (c.size() == 0)
            return null;
        
        int n = (int)(Math.random() * c.size());
        
        return c.get(n);
    }

    /**
     * Returns a random free position in the pasture if there exists
     * one.
     * 
     * If the first random position turns out to be occupied, the rest
     * of the board is searched to find a free position. 
     */
    private Point getFreePosition(Entity toPlace) 
        throws MissingResourceException {
        Point position = new Point((int)(Math.random() * width),
                                   (int)(Math.random() * height)); 

        int p = position.x+position.y*width;
        int m = height * width;
        int q = 97; //any large prime will do

        for (int i = 0; i<m; i++) {
            int j = (p+i*q) % m;
            int x = j % width;
            int y = j / width;

            position = new Point(x,y);
            boolean free = true;

            Collection <Entity> c = getEntitiesAt(position);
            if (c != null) {
                for (Entity thisThing : c) {
                    if(!toPlace.isCompatible(thisThing)) { 
                        free = false; break; 
                    }
                }
            }
            if (free) return position;
        }
        throw new MissingResourceException(
                  "There is no free space"+" left in the pasture",
                  "Pasture", "");
    }
    
            
    public Point getPosition (Entity e) {
        return point.get(e);
    }

    /**
     * Add a new entity to the pasture.
     */
    public void addEntity(Entity entity, Point pos) {

        world.add(entity);

        List<Entity> l = grid.get(pos);
        if (l == null) {
            l = new  ArrayList<Entity>();
            grid.put(pos, l);
        }
        l.add(entity);

        point.put(entity,pos);

        gui.addEntity(entity, pos);
    }
    
    public void moveEntity(Entity e, Point newPos) {
        
        Point oldPos = point.get(e);
        List<Entity> l = grid.get(oldPos);
        if (!l.remove(e)) 
            throw new IllegalStateException("Inconsistent stat in Pasture");
        /* We expect the entity to be at its old position, before we
           move, right? */
                
        l = grid.get(newPos);
        if (l == null) {
            l = new ArrayList<Entity>();
            grid.put(newPos, l);
        }
        l.add(e);

        point.put(e, newPos);

        gui.moveEntity(e, oldPos, newPos);
    }

    /**
     * Remove the specified entity from this pasture.
     */
    public void removeEntity(Entity entity) { 

        Point p = point.get(entity);
        world.remove(entity); 
        grid.get(p).remove(entity);
        point.remove(entity);
        gui.removeEntity(entity, p);

    }

    /**
     * Various methods for getting information about the pasture
     */

    public List<Entity> getEntities() {
        return new ArrayList<Entity>(world);
    }
        
    public Collection<Entity> getEntitiesAt(Point lookAt) {

        Collection<Entity> l = grid.get(lookAt);
        
        if (l==null) {
            return null;
        }
        else {
            return new ArrayList<Entity>(l);
        }
    }


    public List<Point> getFreeNeighbours(Entity entity) {
        List<Point> free = new ArrayList<Point>();

        int entityX = getEntityPosition(entity).x;
        int entityY = getEntityPosition(entity).y;

        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                Point p = new Point(entityX + x,
                          entityY + y);
                if (freeSpace(p, entity))
                    free.add(p);
            }
        }
        return free;
    }

    private boolean freeSpace(Point p, Entity e) {
                              
        List <Entity> l = grid.get(p);
        if ( l == null  ) return true;
        for (Entity old : l ) 
            if (! old.isCompatible(e)) return false;
        return true;
    }

    public Point getEntityPosition(Entity entity) {
        return point.get(entity);
    }


    /** The method for the JVM to run. */
    public static void main(String[] args) {

        new Pasture();
    }


}



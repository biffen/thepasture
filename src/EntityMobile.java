import java.awt.Point;
import java.util.ArrayList;

/**
 * Abstract class used for mobile entities (that can move).
 * It extends the PastureEntity class with stuff that is needed
 * when dealing with moving entitites, like Sheep and Wolf.
 * @author Andreas Andersson, anan8777, andreas@biffnet.se
 * @edited 2015-05-04
 */
public abstract class EntityMobile extends PastureEntity {
	
	protected int mMoveDelay = 0;
	protected int mCurrentCourse = Course.NORTH;
	protected int mTimeToLive = 0;
	protected int mTimeLived = 0;
	protected boolean mHasEaten = false;

	public EntityMobile(Pasture myPasture, int propagationTime,
			String imageUrl, boolean alive, int moveDelay, int timeToLive) {
		super(myPasture, propagationTime, imageUrl, alive);
		mMoveDelay = moveDelay;
		mTimeToLive = timeToLive;
	}
	
	/**
	 * Decreaces the time-to-live counter and returns whether the entity
	 * has starved to death or not. Needs to be run at every tick().
	 * @return True if the entity has starved to death, otherwise false.
	 */
	protected boolean hasStarvedToDeath() {
		mTimeToLive--;
		if(mTimeToLive == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Moves this entity to a random point.
	 */
	protected void goToRandom() {
		
		Point myPos = mPasture.getEntityPosition(this);
		if(mPasture.getFreeNeighbours(this).size() > 0) {
			Point freePoint = mPasture.getFreeNeighbours(this).get(0);
			if(freePoint != null) {
				System.out.println("Moving " + this + " random.");
				mCurrentCourse = Course.getNewCourse(myPos, freePoint);
				mPasture.moveEntity(this, freePoint);
				return;
			}
		}
	}

	/**
	 * Returns the next point in the given direction.
	 * @param myPos The position to start from.
	 * @param myCourse The direction to move in.
	 * @return The point in that direction.
	 */
	protected Point getPoint(Point myPos, int myCourse) {		
		Point destPos = null;
		if(myCourse == Course.NORTH) {
			destPos = new Point(myPos.x, myPos.y-1);
		} else if(myCourse == Course.NORTHEAST) {
			destPos = new Point(myPos.x+1, myPos.y-1);
		} else if(myCourse == Course.EAST) {
			destPos = new Point(myPos.x+1, myPos.y);
		} else if(myCourse == Course.SOUTHEAST) {
			destPos = new Point(myPos.x+1, myPos.y+1);
		} else if(myCourse == Course.SOUTH) {
			destPos = new Point(myPos.x, myPos.y+1);
		} else if(myCourse == Course.SOUTHWEST) {
			destPos = new Point(myPos.x-1, myPos.y+1);
		} else if(myCourse == Course.WEST) {
			destPos = new Point(myPos.x-1, myPos.y);
		} else if(myCourse == Course.NORTHWEST) {
			destPos = new Point(myPos.x-1, myPos.y-1);
		}
		
		return destPos;
	}

	/**
	 * Returns the point that is in the oposite direction of danger.
	 * @param myPos The current position.
	 * @param dangerPos The point that contains the danger.
	 * @return The point that should be safe.
	 */
	protected Point getSafePoint(Point myPos, Point dangerPos) {
		int safeCourse = Course.getNewCourse(myPos, dangerPos);
		safeCourse = Course.reverse(safeCourse);
		Point safePoint = getPoint(myPos, safeCourse);
		
		// Making sure the "safe point" is in fact safe and possible to move to
		if(safePoint != null) {
			ArrayList<Entity> entities = (ArrayList<Entity>) mPasture.getEntitiesAt(safePoint);
			if(entities != null) {
				for(Entity entity : entities) {
					if(!isCompatible(entity)) {
						safePoint = null;
						break;
					}
				}
			}
			
		}
		
		return safePoint;
	}
	
	/**
	 * Returns a list with all points that is in sight, with the closer
	 * ones first.
	 * @param myPos The point to "look from".
	 * @return ArrayList containing all points in sight.
	 */
	protected ArrayList<Point> getPointsInSight(Point myPos, int mySight) {
		
		if(myPos == null) {
			return new ArrayList<Point>();
		}
		
		ArrayList<Point> pointList = new ArrayList<Point>();
		for(int i = 1; i <= mySight; i++) {
			pointList.add(new Point(myPos.x, myPos.y-i)); //North
			pointList.add(new Point(myPos.x+i, myPos.y-i)); //Northeast
			pointList.add(new Point(myPos.x+i, myPos.y));//East
			pointList.add(new Point(myPos.x+i, myPos.y+i));//Southeast
			pointList.add(new Point(myPos.x, myPos.y+i));//South
			pointList.add(new Point(myPos.x-i, myPos.y+i));//Southwest
			pointList.add(new Point(myPos.x-i, myPos.y));//West
			pointList.add(new Point(myPos.x-i, myPos.y-i));//Northwest
		}
		
		return pointList;
	}
	
	/**
	 * Returns a "good point" that is in a similar direction as before.
	 * @param myPos The current position.
	 * @return The good point (or null if none was found)
	 */
	protected Point getSimilarCoursePoint(Point myPos) {
		for(Point thisPoint : getSimilarCoursePoints(myPos)) {
			boolean pointIsOk = false;
			ArrayList<Entity> entityList = (ArrayList<Entity>) mPasture.getEntitiesAt(thisPoint);
			if(entityList == null) {
				pointIsOk = true;
			} else {
				for(Entity thisEntity : mPasture.getEntitiesAt(thisPoint)) {
					if(!isCompatible(thisEntity)) {
						pointIsOk = false;
						break;
					} else {
						pointIsOk = true;
					}
				}
				
			}
			if(pointIsOk) {
				return thisPoint;
			}
		}
		return null;
	}
	
	/**
	 * Determines what points are in a similar direction, 
	 * based on a given position. Will return a list of three
	 * points: 
	 *  - One in the exact same direction
	 *  - One that is slightly to the right of the course
	 *  - One that is slightly to the left of the course 
	 * @param myPos The current position.
	 * @return The list of three points.
	 */
	protected ArrayList<Point> getSimilarCoursePoints(Point myPos) {
		
		// This gigantic if-else will add all points the sheep wants to go 
		//to, that is in a similar course as before, to a list. 
		ArrayList<Point> possiblePoints = new ArrayList<Point>();
		if(mCurrentCourse == Course.NORTH) {
			possiblePoints.add(new Point(myPos.x, myPos.y-1));
			possiblePoints.add(new Point(myPos.x+1, myPos.y-1));
			possiblePoints.add(new Point(myPos.x-1, myPos.y-1));
		}
		else if(mCurrentCourse == Course.NORTHEAST) {
			possiblePoints.add(new Point(myPos.x+1, myPos.y-1));
			possiblePoints.add(new Point(myPos.x+1, myPos.y));
			possiblePoints.add(new Point(myPos.x, myPos.y-1));
		}
		else if(mCurrentCourse == Course.EAST) {
			possiblePoints.add(new Point(myPos.x+1, myPos.y));
			possiblePoints.add(new Point(myPos.x+1, myPos.y+1));
			possiblePoints.add(new Point(myPos.x+1, myPos.y-1));
		}
		else if(mCurrentCourse == Course.SOUTHEAST) {
			possiblePoints.add(new Point(myPos.x+1, myPos.y+1));
			possiblePoints.add(new Point(myPos.x, myPos.y+1));
			possiblePoints.add(new Point(myPos.x+1, myPos.y));
		}
		else if(mCurrentCourse == Course.SOUTH) {
			possiblePoints.add(new Point(myPos.x, myPos.y+1));
			possiblePoints.add(new Point(myPos.x-1, myPos.y+1));
			possiblePoints.add(new Point(myPos.x+1, myPos.y+1));
		}
		else if(mCurrentCourse == Course.SOUTHWEST) {
			possiblePoints.add(new Point(myPos.x-1, myPos.y+1));
			possiblePoints.add(new Point(myPos.x-1, myPos.y));
			possiblePoints.add(new Point(myPos.x, myPos.y+1));
		}
		else if(mCurrentCourse == Course.WEST) {
			possiblePoints.add(new Point(myPos.x-1, myPos.y));
			possiblePoints.add(new Point(myPos.x-1, myPos.y-1));
			possiblePoints.add(new Point(myPos.x-1, myPos.y+1));
		}
		else if(mCurrentCourse == Course.NORTHWEST) {
			possiblePoints.add(new Point(myPos.x-1, myPos.y-1));
			possiblePoints.add(new Point(myPos.x, myPos.y-1));
			possiblePoints.add(new Point(myPos.x-1, myPos.y));
		}
		return possiblePoints;
	}

	@Override
	public void tick() {
		if(mAlive) {
			mTimeLived++;
			mMoveDelay--;
			mPropagationTime--;
			
			if(mHasEaten && mPropagationTime <= 0) {
				// Spawn new & reset timer
				Point freeNeighbour = null;
				for(Point pointInSight : getPointsInSight(mPasture.getEntityPosition(this), 1)) {
					boolean goodPoint = false;
					for(Entity thisEntity : mPasture.getEntitiesAt(pointInSight)) {
						if(!(thisEntity instanceof Plant)) {
							goodPoint = false;
							break;
						} else {
							goodPoint = true;
						}
					}
					if(goodPoint) {
						freeNeighbour = pointInSight;
						break;
					}
				}
				if(freeNeighbour == null) {
					return;
				}
				if(this instanceof Sheep) {
					Sheep newSheep = new Sheep(mPasture, mPasture.PROPAGATION_TIME_SHEEP);
					mPasture.addEntity(newSheep, freeNeighbour);
					mPropagationTime = mPasture.PROPAGATION_TIME_SHEEP;
					System.out.println("Spawning sheep");
				} else if(this instanceof Wolf) {
					Wolf newWolf = new Wolf(mPasture, mPasture.PROPAGATION_TIME_WOLF);
					mPasture.addEntity(newWolf,freeNeighbour);
					mPropagationTime = mPasture.PROPAGATION_TIME_WOLF;
					System.out.println("Spawning wolf");
				}
			}
		}
	}

	@Override
	public abstract boolean isCompatible(Entity otherEntity);

}

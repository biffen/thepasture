import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class with static methods that is used to do different operations
 * concerning what course the entity is moving in. 
 * @author Andreas Andersson, anan8777, andreas@biffnet.se
 * @edited 2015-05-04
 */
public class Course {
	
	public static final int NORTH = 0;
	public static final int NORTHEAST = 1;
	public static final int EAST = 2;
	public static final int SOUTHEAST = 3;
	public static final int SOUTH = 4;
	public static final int SOUTHWEST = 5;
	public static final int WEST = 6;
	public static final int NORTHWEST = 7;
	
	/**
	 * Helper function that is useful if you want to go through
	 * all or some of the courses. It will take the old course
	 * as input and return the next one in the list (or the first
	 * one if you were on the last one).
	 * @param oldCourse The current course.
	 * @return The next course in the list.
	 */
	public static int next(int oldCourse) {
		int newCourse = oldCourse+1;
		if(newCourse > NORTHWEST) {
			newCourse = NORTH;
		}
		return newCourse;
	}
	
	/**
	 * Creates an ArrayList with the integers representing courses 
	 * and shuffles it. Used when Entities needs to go in a random
	 * direction.
	 * @return ArrayList<Integer> with all courses in random order.
	 */
	public static ArrayList<Integer> random() {
		
		ArrayList<Integer> directionsList = new ArrayList<Integer>();
		directionsList.add(NORTH);
		directionsList.add(NORTHEAST);
		directionsList.add(EAST);
		directionsList.add(SOUTHEAST);
		directionsList.add(SOUTH);
		directionsList.add(SOUTHWEST);
		directionsList.add(WEST);
		directionsList.add(NORTHWEST);
		
		Collections.shuffle(directionsList);
		
		return directionsList;
	}
	
	/**
	 * Takes two points and returns what drection an entity needs
	 * to go in to get from oldPos to newPos.
	 * @param oldPos The old position.
	 * @param newPos The new position.
	 * @return The direction needed to go from the old to the new
	 * direction (or north if something went wrong).
	 */
	public static int getNewCourse(Point oldPos, Point newPos) {
		if(newPos.x == oldPos.x && newPos.y < oldPos.y) {
			return Course.NORTH;
		} else if(newPos.x > oldPos.x && newPos.y < oldPos.y) {
			return Course.NORTHEAST;
		} else if(newPos.x > oldPos.x && newPos.y == oldPos.y) {
			return Course.EAST;
		} else if(newPos.x > oldPos.x && newPos.y > oldPos.y) {
			return Course.SOUTHEAST;
		} else if(newPos.x == oldPos.x && newPos.y > oldPos.y) {
			return Course.SOUTH;
		} else if(newPos.x < oldPos.x && newPos.y > oldPos.y) {
			return Course.SOUTHWEST;
		} else if(newPos.x < oldPos.x && newPos.y == oldPos.y) {
			return Course.WEST;
		} else if(newPos.x < oldPos.x && newPos.y < oldPos.y) {
			return Course.NORTHWEST;
		}
		
		return Course.NORTH;
	}
	
	/**
	 * Given a course as input it will return the reverse course.
	 * @param oldCourse The given course.
	 * @return The reverse course (or north if invalid given course).
	 */
	public static int reverse(int oldCourse) {
		switch(oldCourse) {
		case NORTH:
			return SOUTH;
		case NORTHEAST:
			return SOUTHWEST;
		case EAST:
			return WEST;
		case SOUTHEAST:
			return NORTHWEST;
		case SOUTH:
			return NORTH;
		case SOUTHWEST:
			return NORTHEAST;
		case WEST:
			return EAST;
		case NORTHWEST:
			return SOUTHEAST;
		default:
			return NORTH;
		}
	}
	
}

import javax.swing.*;

/**
 * This is the superclass of all entities in the pasture simulation
 * system. This interface <b>must</b> be implemented by all entities
 * that exist in the simulation of the pasture.
 */
public interface Entity {

	/**
	 * This method is run every "cycle" and takes care of all
	 * decision-making in the entity.
	 */
    public void tick();

    /** 
     * ImageIcon returns the icon of this entity, to be displayed by
     * the pasture gui.
     */
    public ImageIcon getImage();
    
	/**
     * Tests if this entity can be on the same position in the pasture
     * as the given one.
     */
    
    /**
     * Tests if this entity can be on the same position in the pasture
     * as the given one.
     * @param otherEntity The given entity that this entity can or cannot
     * be on the same place as.
     * @return True if they can be on the same point, otherwise false.
     */
    public boolean isCompatible(Entity otherEntity);

}

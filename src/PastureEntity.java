import javax.swing.ImageIcon;

/**
 * The top-level class that implements the Entity interface.
 * Contains only what ALL entities needs.
 * @author Andreas Andersson, anan8777, andreas@biffnet.se
 * @edited 2015-05-04
 */
public abstract class PastureEntity implements Entity {
	
	protected ImageIcon mImage = null;
	protected Pasture mPasture = null;
	protected boolean mAlive = false;
	protected int mPropagationTime = 0;
	
	public PastureEntity(Pasture myPasture, int propagationTime, String imageUrl, boolean alive) 
	{
		mPasture = myPasture;
		mImage = new ImageIcon(imageUrl); 
		mAlive = alive;
		mPropagationTime = propagationTime;
	}

	@Override
	public ImageIcon getImage()
	{
		return mImage;
	}
	
	@Override
	public abstract void tick();

	@Override
	public abstract boolean isCompatible(Entity otherEntity);

}

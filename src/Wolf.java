import java.awt.Point;
import java.util.ArrayList;

/**
 * Represents a Wolf in the pasture.
 * @author Andreas Andersson, anan8777, andreas@biffnet.se
 * @edited 2015-05-04
 */
public class Wolf extends EntityMobile {
	
	private final static String IMAGE_URL = "images/wolf.gif";
	private final static boolean IS_ALIVE = true; 

	public Wolf(Pasture myPasture, int propagationTime) {
		super(myPasture, propagationTime, IMAGE_URL, IS_ALIVE, 
				Pasture.MOVE_DELAY_WOLF, Pasture.STARVE_TIME_WOLF);
	}

	@Override
	public void tick() {

		Point myPos = mPasture.getPosition(this);
		
		// Eat Sheep if there are any
		ArrayList<Entity> entitesAtThisPos = (ArrayList<Entity>) mPasture.getEntitiesAt(myPos);
		if(entitesAtThisPos != null) {
			for(Entity thisEntity : mPasture.getEntitiesAt(myPos)) {
				if(thisEntity instanceof Sheep) {
					mPasture.removeEntity(thisEntity);
					mTimeToLive = Pasture.STARVE_TIME_WOLF;
					mHasEaten = true;
				}
			}
		}
		
		// Checks whether this wolf has starved to death or not
		if(hasStarvedToDeath()) {
			System.out.println("Wolf died of starvation.");
			mPasture.removeEntity(this);
			return;
		}
		
		// One time unit has passed
		super.tick();
		
		
		
		// If its not my time, then do nothing
		if(mMoveDelay != 0) {
			return;
		} else {
			mMoveDelay = Pasture.MOVE_DELAY_WOLF;
		}
		
		// If a Sheep is within sight, move towards it
		ArrayList<Point> pointsInSight = getPointsInSight(myPos, mPasture.SIGHT_LENGTH_WOLF);
		for(Point newPoint : pointsInSight) {
			ArrayList<Entity> pointList = (ArrayList<Entity>) mPasture.getEntitiesAt(newPoint);
			if(pointList != null) {
				for(Entity thisEntity : pointList) {
					if(thisEntity instanceof Sheep) {
						Point sheepPos = mPasture.getEntityPosition(thisEntity);
						mCurrentCourse =  Course.getNewCourse(myPos, sheepPos);
						Point newDest = getPoint(myPos, mCurrentCourse);
						mPasture.moveEntity(this, newDest);
						System.out.println("Moving " + this + " towards " + thisEntity + ".");
						return;
					}
				}
			}
		}
		
		// If there are no sheep in sight, move in a similar direction
		Point similarCoursePoint = getSimilarCoursePoint(myPos);
		if(similarCoursePoint != null) {
			mCurrentCourse =  Course.getNewCourse(myPos, similarCoursePoint);
			mPasture.moveEntity(this, similarCoursePoint);
			System.out.println("Moving " + this + " similar course.");
			return;
		}
		
		// If its not possible to move in i similar direction, move in a 
		// random direction
		goToRandom();
		
		System.out.println("WOLF DONE");
	}

	@Override
	public boolean isCompatible(Entity otherEntity) {
		if(otherEntity instanceof Fence || otherEntity instanceof Wolf ) {
			return false;
		} else {
			return true;
		}
	}

}

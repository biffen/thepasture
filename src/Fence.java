
public class Fence extends PastureEntity {
	
	private final static String FENCE_IMAGE_URL = "images/fence.gif";
	private final static boolean IS_ALIVE = false; 

	public Fence(Pasture myPasture) {
		super(myPasture, 0, FENCE_IMAGE_URL, IS_ALIVE);
	}

	@Override
	public void tick() { /* I do nothing */ }

	@Override
	public boolean isCompatible(Entity otherEntity) {
		// You shall not pass!
		return false;
	}

}
